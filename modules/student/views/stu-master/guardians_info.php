<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\student\models\StuMaster */
/*
*/

$this->title = 'Update Marks card : ' . ' ' ;
?>
<script>
$(document).ready(function(){
	$('.modal-header').html('<div class="row"><div class="col-xs-12"><button class="close" aria-hidden="true" data-dismiss="modal" type="button">×</button><div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?> </h3></div></div></div>');
});

</script>

<div class="col-xs-12 col-lg-12 no-padding">
  <div class="box-info box view-item col-xs-24 col-lg-24">
   <div class="stu-master-update">

    <?php $form = ActiveForm::begin([
			'id' => 'stu-master-update',
			'enableAjaxValidation' => true,
			'fieldConfig' => [
			    'template' => "{label}{input}{error}",
			],	
    ]); ?>

       <table>
               <!-- sem row-->
               <tr>
                   <th></th>
                   <th colspan="4" > sem 1</th>
                    <th colspan="4">sem 2</th>
                     <th rowspan="2">TOTAL GRADE</th>
               </tr>
                <!-- subject, test row row-->
  <tr>
    <th>Subjects</th>
    <th>FA 1</th>
    <th>FA 2</th>
    <th>SA 1</th>
    <th>Total</th>
    <th>FA 3</th>
    <th>FA 4</th>
    <th>SA 2</th>
    <th>TOTAL</th>
   
  </tr>
   <!-- percentage row-->
   <tr>
       <th> </th>
    <th>10%</th>
    <th>10%</th>
    <th>30%</th>
    <th>50%</th>
    <th>10%</th>
    <th>10%</th>
    <th>30%</th>
    <th>50%</th>
    <th>100%</th>
   
  </tr>
   <!-- First language row row-->
  <tr>
    <td>First Language</td>
    <td>  <?= $form->field($guard, 'guardian_phone_no')->textInput() ?></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Secound Language Row -->
  <tr>
    <td>Secound Language</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Third Language row-->
  <tr>
    <td>Third Language</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Mathematics row-->
  <tr>
    <td>Mathematics</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Science row-->
  <tr>
    <td>Science</td>
     <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Social Science row-->
  <tr>
    <td>Social Science</td>
     <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Physical and health edication row-->
  <tr>
    <td>Physical and Health Education</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <!-- Drawing and work experience row-->
  <tr>
    <td>Drawing And Work Experience</td>
     <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <!-- computer education row-->
  <tr>
    <td>Computer Education</td>
     <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
       <table>
            <tr>
                <th colspan="3"><h3>Co - Scholastic achievement</h3></th>
            </tr>
            <tr>
                <th>Description</th>
                <th>First Sem Grade</th>
                <th>Secound Sem Grade</th>
            </tr>
            <tr>
                <td>Emotional And Social Skills</td>
                <td></td>
                <td></td>
               
            </tr>
             <tr>
               
                <td>Organisational Skills</td>
                <td></td>
                <td></td>
                
            </tr>
             <tr>
               
                <td>Scientific Skills</td>
                <td></td>
                <td></td>
               
            </tr>
             <tr>
               
                <td>Fine Arts</td>
                <td></td>
                <td></td>
               
            </tr>
             <tr>
               
                <td>Creativity</td>
                <td></td>
                <td></td>
            </tr>
             <tr>
               
                <td>Attitude</td>
                <td></td>
                <td></td>
            </tr>
             <tr>
               
                <td>Value</td>
                <td></td>
                <td></td>
            </tr>
           
        </table>

    <div class="form-group col-xs-12 col-sm-6 col-lg-6 no-padding">
	<div class="col-xs-6 col-sm-6">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord  ? 'btn btn-block btn-success' : 'btn btn-block btn-info']) ?>
	</div>
	<!--div class="col-xs-6">
	    <?php // Html::a('Cancel', ['view', 'id' => $model->stu_master_id], ['class' => 'btn btn-default btn-block']); ?>
	</div-->
    </div>

    <?php ActiveForm::end(); ?>
   </div>
  </div>
</div>
