<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\helpers\Url;
use app\models\Languages;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\student\models\StuMaster */

$this->title = 'Update Personal Details : ' . ' ' . $model->stuMasterStuInfo->getName();
$this->params['breadcrumbs'][] = ['label' => 'Student', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Manage Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->stuMasterStuInfo->getName(), 'url' => ['view', 'id' => $model->stu_master_id]];
$this->params['breadcrumbs'][] = 'Update Personal Details';
?>
<style>
.box .box-solid {
     background-color: #F8F8F8;
}
</style>
<script>
$(function () {
  $('[data-toggle="popover"]').popover({placement: function() { return $(window).width() < 768 ? 'bottom' : 'right'; }})
})
</script>

<div class="col-xs-12">
  <div class="col-lg-8 col-sm-8 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?> </h3>
  </div>
  <div class="col-xs-4"></div>
  <div class="col-lg-4 col-sm-4 col-xs-12 no-padding" style="padding-top: 20px !important;">
	<div class="col-xs-4"></div>
	<div class="col-xs-4"></div>
	<div class="col-xs-4 left-padding">
	<?= Html::a('Back', ['view', 'id' => $model->stu_master_id], ['class' => 'btn btn-block btn-back']) ?>
	</div>
  </div>
</div>

<div class="col-xs-12 col-lg-12">
  <div class="box-info box view-item col-xs-12 col-lg-12">
   <div class="stu-master-update">

    <?php $form = ActiveForm::begin([
			'id' => 'stu-master-update',
			'fieldConfig' => [
			    'template' => "{label}{input}{error}",
			],
    ]); ?>
    <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
      <div class="box-header with-border">
         <h4 class="box-title"><i class="fa fa-info-circle"></i> Personal Details</h4>
      </div>
    <div class="box-body">
        
         <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'stu_dice_num')->textInput() ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'stu_adm_num')->textInput() ?>
		
    </div>
    

   </div>
        
        

   <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
    <div class="col-xs-9 col-sm-4">
	<!--<?= $form->field($info, 'stu_unique_id')->textInput() ?>-->
    </div>
    <div class="col-xs-3 col-sm-8" style="padding-top: 25px;">
	<?php $stu_login_prefix = app\models\Organization::find()->one()->org_stu_prefix;?>
	<!--<button type="button" class="btn btn-danger" data-html=true data-toggle="popover" title="Student Login Note" data-trigger="focus" data-content="Unique Id is used as login username with <b><?= $stu_login_prefix ?> </b>prefix. </br> Example: If Unique id : 123 so, Username : <?= $stu_login_prefix ?>123"><i class="fa fa-info-circle"></i></button>-->
    </div>
   </div>

   <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
   <!-- <div class="col-xs-12 col-sm-4 col-lg-4">
	<?= $form->field($info, 'stu_title')->dropDownList($info->getTitleOptions(),['prompt'=>'---Select Title---']); ?>
    </div>-->
   </div>

   <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'stu_first_name')->textInput() ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'stu_middle_name')->textInput() ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	 <?= $form->field($info, 'stu_last_name')->textInput() ?>
    </div>

   </div>

   <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
    <div class="col-xs-12 col-sm-4 col-lg-4">
   	<?= $form->field($info, 'stu_gender')->dropDownList(['' => '---Select Gender---', 'Male' => 'Male', 'Female' => 'Female']) ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	 <?= $form->field($info, 'stu_email_id')->textInput(['maxlength' => 65]) ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'stu_mobile_no')->textInput(['maxlength' => 12]) ?>
    </div>
   </div>

   <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'stu_dob')->widget(yii\jui\DatePicker::className(),
                    [
                      'clientOptions' =>[
                          'dateFormat' => 'dd-mm-yyyy',
                          'changeMonth'=> true,
			  'yearRange'=>'1900:'.(date('Y')+1),
                          'changeYear'=> true,
			  'readOnly'=>true,
                          'autoSize'=>true,
                          'buttonImage'=> Yii::$app->homeUrl."images/calendar.png",],
                      'options'=>[
			  'class'=>'form-control',
                         ],]) ?>
   
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    <?= $form->field($model, 'stu_master_category_id')->dropDownList(ArrayHelper::map(app\modules\student\models\StuCategory::find()->where(['is_status' => 0])->all(),'stu_category_id','stu_category_name'),['prompt'=>'---Select Category---']); ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    <?= $form->field($model, 'stu_master_nationality_id')->dropDownList(ArrayHelper::map(app\models\Nationality::find()->where(['is_status' => 0])->all(),'nationality_id','nationality_name'),['prompt'=>'---Select Nationality---']); ?>
    </div>
   </div>

   <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'stu_birthplace')->textInput(['maxlength' => 45]) ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    <?= $form->field($info, 'stu_bloodgroup')->dropDownList($info->getBloodGroup()); ?>
    </div>

    <div class="col-xs-12 col-sm-4 col-lg-4">
    <?= $form->field($info, 'stu_religion')->textInput(['maxlength' => 50]) ?>
    </div>
   </div>
        
    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'mother_tongue')->textInput(['maxlength' => 45]) ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    <?= $form->field($info, 'taluk')->textInput(['maxlength' => 45]) ?>
    </div>

    <div class="col-xs-12 col-sm-4 col-lg-4">
    <?= $form->field($info, 'district')->textInput(['maxlength' => 50]) ?>
    </div>
   </div>
        
        <!--father names -->
     <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'father_first_name')->textInput() ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'father_middle_name')->textInput() ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	 <?= $form->field($info, 'father_last_name')->textInput() ?>
    </div>
   </div>
        
   <!--mother names -->
     <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'mother_first_name')->textInput() ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'mother_middle_name')->textInput() ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	 <?= $form->field($info, 'mother_last_name')->textInput() ?>
    </div>
   </div>
   
   <!--Income names -->
     <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'annual_income')->textInput() ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	<?= $form->field($info, 'income_category')->textInput() ?>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-4">
    	 <?= $form->field($info, 'no_dependents')->textInput() ?>
    </div>
   </div>


   <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
   <!-- <div class="col-xs-12 col-sm-12">
    <?php $data = ArrayHelper::map(Languages::find()->asArray()->all(), 'language_name', 'language_name');
	foreach($data as $d)
		$langss[]=$d;

	echo $form->field($info, 'stu_languages')->widget(Select2::classname(),[
			'name' => 'stu_languages[]',
			'options' => ['placeholder' => ''],
			'pluginOptions' => [
				'tags' => $langss,
				'maximumInputLength' => 10,
				'multiple' => true
			],
		]); 
	?>
    </div> -->
   </div> 

    </div> <!--/ box-body -->
    </div> <!--/ box -->
    
  <!-- Contact Information -->
  <div class="box box-solid box-success col-xs-12 col-lg-12 no-padding">
  <div class="box-header with-border">
    <h4 class="box-title"><i class="fa fa-info-circle"></i> Contact Information</h4>
   </div>
        <div class="box-body">
             <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'house_no')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?php // $form->field($info, 'house_name')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'street_name')->textInput() ?>
                  </div>
             </div>
             <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                 <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                  
                     
   	<div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'country')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'state')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'city')->textInput() ?>
                  </div>
            
              <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                   <div class="col-xs-12 col-sm-1 col-lg-1">
                      <?= $form->field($info, 'contact_landline_std')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'phone')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-1 col-lg-1">
                      <?= $form->field($info, 'contact_fax_std')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'fax_no')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'office_no')->textInput() ?>
                  </div>
                  
             </div>
            <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                   <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'pincode')->textInput() ?>
                  </div>
                <div class="col-xs-12 col-sm-4 col-lg-4">
                    <?= $form->field($info, 'stu_email_id')->textInput(['maxlength' => '60']) ?>
                 </div>
                 <div class="col-xs-12 col-sm-4 col-lg-4">
                    <?= $form->field($info, 'stu_mobile_no')->textInput(['maxlength' => '12']) ?>
                 </div>
            </div>
        </div>
   
</div>
             </div>
        </div>
   
</div>
  
<!-- guardian details -->
<div class="box box-solid box-danger col-xs-12 col-lg-12 no-padding">
  <div class="box-header with-border">
    <h4 class="box-title"><i class="fa fa-info-circle"></i> Guardian Details</h4>
  </div>
    <div class="box-body">
         <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'g_name')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'g_relation')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'g_occupation')->textInput() ?>
                  </div>
                  
             </div>
        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'g_qulification')->textInput() ?>
                  </div>
             
        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'g_city')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'g_state')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'g_country')->textInput() ?>
                  </div>
                  
             </div>
        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
            <div class="col-xs-12 col-sm-1 col-lg-1">
                      <?= $form->field($info, 'guardian_landline_std')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'g_phonenum')->textInput() ?>
                  </div>
             <div class="col-xs-12 col-sm-2 col-lg-2">
                      <?= $form->field($info, 'guardian_country_mobile')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'g_mobilenum')->textInput() ?>
                  </div>
                  
             </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'g_homeaddress')->textarea() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'g_officeaddress')->textarea() ?>
                  </div>
                 
        </div>
     </div>
</div>
 

<!-- Emergency Contact details-->
<div id="thisWrapper">
    <input type="checkbox" id="check-1" class="checkchoice" ><label for="check-1" class="choice staff">Same as above</label>
   
</div>
  <div id="hide">
 <div class="box box-solid box-primary col-xs-12 col-lg-12 no-padding">
  <div class="box-header with-border">
    <h4 class="box-title"><i class="fa fa-info-circle"></i> Emergency Contact</h4>
  </div>
    <div class="box-body">
         <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'e_firstname')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'e_middle_name')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'e_last_name')->textInput() ?>
                  </div>
                  
             </div>
        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
<!--                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'e_housenum')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'e_housename')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'e_streetname')->textInput() ?>
                  </div>-->
                  
             </div>
        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'e_city')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'e_state')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'e_country')->textInput() ?>
                  </div>
             </div>
       
         <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                   <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'e_pincode')->textInput() ?>
                  </div>
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'e_address')->textArea() ?>
                  </div>
        </div>
      
        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
            <div class="col-xs-12 col-sm-1 col-lg-1">
                      <?= $form->field($info, 'emergency_landline_std')->textInput() ?>
                
                  </div>
            &minus;
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      
                      <?= $form->field($info, 'e_phonenum')->textInput() ?>
                  </div>
           
             <div class="col-xs-12 col-sm-2 col-lg-2">
                      <?= $form->field($info, 'emergency_country_mobile')->textInput() ?>
                 
                  </div>
           
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'e_mobilenum')->textInput() ?>
                  </div>
        </div>
     </div>
</div>

  
  
  
  <div class="box box-solid box-success col-xs-12 col-lg-12 no-padding">
  <div class="box-header with-border">
    <h4 class="box-title"><i class="fa fa-info-circle"></i> File Number and Date</h4>
  </div>
  <div class="box-body">
    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                  <div class="col-xs-12 col-sm-4 col-lg-4">
                      <?= $form->field($info, 'file_number')->textInput() ?>
                  </div>
                <div class="col-xs-12 col-sm-4 col-lg-4">
		<?= $form->field($info, 'upload_date')->widget(yii\jui\DatePicker::className(),
	            [
			'model'=>$info,
			'attribute'=>'upload_date', 
	            'clientOptions' =>[
	                'dateFormat' => 'dd-mm-yyyy',
	                'changeMonth'=> true,
			'yearRange'=>'1900:'.(date('Y')+1),
	                'changeYear'=> true,
			'readOnly'=>true,
	                'autoSize'=>true,],
	            'options'=>[
			'class'=>'form-control',
	                 ],]) ?>		
	</div> 
                 
    </div> 
     </div>
  </div>
    <div class="form-group col-xs-12 col-sm-6 col-lg-4 no-padding">
	<div class="col-xs-6">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord  ? 'btn btn-block btn-success' : 'btn btn-block btn-info']) ?>
	</div>
	<div class="col-xs-6">
	    <?= Html::a('Cancel', ['view', 'id' => $model->stu_master_id], ['class' => 'btn btn-default btn-block']); ?>
	</div>
         <div class="box box-solid box-title col-xs-12 col-lg-12 no-padding">
  <div class="box-header with-border">
    <h4 class="box-title"><i class="fa fa-info-circle"></i>Login Number, used for student login, AUTO GENERATED do not alter</h4>
   </div>
      
          <div class="col-xs-9 col-sm-4">
	
	<?= $form->field($info, 'stu_unique_id')->textInput() ?>
    </div>
        </div>
    </div>
	<?php ActiveForm::end(); ?>
   </div>
  </div>
</div>
