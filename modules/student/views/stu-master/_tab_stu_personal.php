
<?php
use yii\helpers\Html; 
$adminUser = array_keys(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()));
?>

<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> Personal Details
	<div class="pull-right">
	<?php if((Yii::$app->user->can("/student/stu-master/update") && ($_REQUEST['id'] == Yii::$app->session->get('stu_id'))) || (in_array("Clerk", $adminUser)) || Yii::$app->user->can("updateAllStuInfo")) { ?>
		<?= Html::a('<i class="fa fa-pencil-square-o"></i> Edit', ['update', 'sid' => $model->stu_master_id, 'tab' => 'personal'], ['class' => 'btn btn-primary btn-sm', 'id' => 'update-data']) ?>
	<?php } ?>
	</div>
	</h2>
  </div><!-- /.col -->
</div>






<div class="row">
    

    
<!--	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('stu_title') ?></div>
		<div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= ($info->stu_title) ? $info->stu_title : "Not Set" ?></div>
	</div> -->

	<div class="col-md-12 col-xs-12 col-sm-12">
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('stu_first_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->stu_first_name ?></div>
	  </div>
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('stu_last_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->stu_last_name ?></div>
	  </div>
	</div>

	<div class="col-md-12 col-xs-12 col-sm-12">
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('stu_middle_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->stu_middle_name ?></div>
	  </div>
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('stu_gender') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->stu_gender ?></div>
	  </div>
	</div>

	<div class="col-md-12 col-xs-12 col-sm-12">
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('stu_dob') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= Yii::$app->formatter->asDate($info->stu_dob); ?></div>
	  </div>
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('stu_master_nationality_id') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= !empty($model->stuMasterNationality->nationality_name) ? $model->stuMasterNationality->nationality_name : "Not Set" ?></div>
	  </div>
	</div>


	<div class="col-md-12 col-xs-12 col-sm-12">
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('stu_master_category_id') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= !empty($model->stuMasterCategory->stu_category_name) ? $model->stuMasterCategory->stu_category_name : "Not Set" ?></div>
	  </div>
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('stu_religion') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->stu_religion ?></div>
	  </div>
	</div>

	<div class="col-md-12 col-xs-12 col-sm-12">
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('stu_bloodgroup') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->stu_bloodgroup ?></div>
	  </div>
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('stu_birthplace') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->stu_birthplace ?></div>
	  </div>
	</div>

	
        <!--<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('stu_languages') ?></div>
		<div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= $info->stu_languages ?></div>
	</div> -->

    <div class="col-md-12 col-xs-12 col-sm-12">
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('taluk') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->taluk ?></div>
	  </div>
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('district') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->district ?></div>
	  </div>
         <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('mother_tongue') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->mother_tongue ?></div>
	  </div>
         <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('father_first_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->father_first_name ?></div>
	  </div> <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('father_middle_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->father_middle_name ?></div>
	  </div> <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('father_last_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->father_last_name ?></div>
	  </div> <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('mother_first_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->mother_first_name ?></div>
	  </div> <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('mother_middle_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->mother_middle_name ?></div>
	  </div> <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('mother_last_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->mother_last_name ?></div>
	  </div>
         <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('annual_income') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->annual_income ?></div>
	  </div>
         <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('income_category') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->income_category ?></div>
	  </div>
         <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('no_dependents') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->no_dependents ?></div>
	  </div>
       
	</div>

    <!-- student additional fields -->
    
   
</div> <!---Main Row Div--->
<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> contact Information
</h2>
<div class ="row">
<div class="col-md-12 col-xs-12 col-sm-12">
<!--
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('house_no') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->house_no ?></div>
	  </div>
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('house_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->house_name ?></div>
	  </div>
     <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('street_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->street_name ?></div>
	  </div>
    -->
     <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('country') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->country ?></div>
	  </div>
     <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('state') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->state ?></div>
	  </div>
     <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('city') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->city ?></div>
	  </div>
	  
	
     <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
         
		
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('phone') ?></div>
                <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->contact_landline_std ?></div>
                
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->phone ?></div>
	  </div>
     <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
         
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('fax_no') ?></div>
                <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->fax_no ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->fax_no ?></div>
	  </div>
     <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('office_no') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->office_no ?></div>
	  </div>
     <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('pincode') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->pincode ?></div>
	  </div>
</div>
</div>

<!--GUARDIAN DETAILS -->
<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> Guardian Details
</h2>
<div class ="row">
<div class="col-md-12 col-xs-12 col-sm-12">
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('g_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->g_name ?></div>
	  </div>
          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('g_relation') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->g_relation ?></div>
	  </div>
           <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('g_qulification') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->g_qulification  ?></div>
	  </div>
           <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('g_occupation') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->g_occupation ?></div>
	  </div>
           <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('g_phonenum') ?></div>
                <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->guardian_landline_std ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->g_phonenum ?></div>
	  </div>
           <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('g_mobilenum') ?></div>
                <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->guardian_country_mobile ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->g_mobilenum ?></div>
	  </div>
           <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('g_city') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->g_city ?></div>
	  </div>
           <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('g_state') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->g_state ?></div>
	  </div>
           <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('g_country') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->g_country ?></div>
	  </div>
	
           <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('g_homeaddress') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->g_homeaddress ?></div>
	  </div>
           <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('g_officeaddress') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->g_officeaddress ?></div>
	  </div>
</div>
</div>

<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> Emergency Contact
</h2>
<div class ="row">
<div class="col-md-12 col-xs-12 col-sm-12">
	  <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_firstname') ?></div>
                
		<div class="col-lg-6 col-xs-6 edusec-profile-text">
                    <?php if($info->e_middle_name == NULL){ ?>
                    <?= $info->father_first_name ?>
                    <?php } else {?>
                     <?= $info->e_firstname ?>
                     <?php } ?>
                </div>
	  </div>
          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_middle_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text">
                    <?php if($info->e_middle_name == NULL){ ?>
                    <?= $info->father_middle_name ?>
                    <?php } else {?>
                     <?= $info->e_middle_name ?>
                     <?php } ?>
                </div>
	  </div>
          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_last_name') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text">
                    <?php if($info->e_last_name == NULL){ ?>
                    <?= $info->father_last_name ?>
                    <?php } else {?>
                     <?= $info->e_last_name ?>
                     <?php } ?>
                </div>
	  </div>
<!--          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_housenum') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->e_housenum ?></div>
	  </div>
          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_housename') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->e_housename ?></div>
	  </div>
          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_streetname') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->e_streetname ?></div>
	  </div>-->
          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_city') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text">
		 <?php if($info->e_city == NULL){ ?>
                    <?= $info->city?>
                    <?php } else {?>
                     <?= $info->e_city ?>
                     <?php } ?>
		</div>
	  </div>
          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_state') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text">
		 <?php if($info->e_state == NULL){ ?>
                    <?= $info->state  ?>
                    <?php } else {?>
                     <?= $info->e_state ?>
                     <?php } ?>
		</div>
	  </div>
          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_country') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text">
		 <?php if($info->e_country == NULL){ ?>
                    <?= $info->country  ?>
                    <?php } else {?>
                     <?= $info->e_country ?>
                     <?php } ?>
		</div>
	  </div>
          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_phonenum') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text">
                    <?php if($info->e_phonenum == NULL){ ?>
                   
                    <?= $info->office_no ?>
                    <?php } else {?>
                     <?= $info->emergency_landline_std ?>
                     <?= $info->e_phonenum ?>
                     <?php } ?>
                </div>
	  </div>
          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_mobilenum') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text">
                    <?php if($info->e_mobilenum == NULL){ ?>
                    <?= $info->contact_landline_std ?>
                    <?= $info->phone ?>
                    <?php } else {?>
                     <?= $info->emergency_country_mobile ?>
                     <?= $info->e_mobilenum ?>
                     <?php } ?>
                </div>
	  </div>
          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_pincode') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text">
                    <?php if($info->e_pincode == NULL){ ?>
                    <?= $info->pincode ?>
                    <?php } else {?>
                     <?= $info->e_pincode ?>
                     <?php } ?>
                </div>
	  </div>
          <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('e_address') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text">
                    <?php if($info->e_address == NULL){ ?>
                    <?= $info->house_name ?>
                    <?php } else {?>
                     <?= $info->e_address ?>
                     <?php } ?>
                </div>
	  </div>
</div>
</div>
<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> File Number and File Date
</h2>
<div class ="row">
<div class="col-md-12 col-xs-12 col-sm-12">
 <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('file_number') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $info->file_number ?></div>
	  </div>
      <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
		<div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $info->getAttributeLabel('upload_date') ?></div>
		<div class="col-lg-6 col-xs-6 edusec-profile-text"><?= Yii::$app->formatter->asDate($info->upload_date); ?></div>
	  </div>
</div>
</div>
    