<?php use yii\helpers\Html;
$adminUser = array_keys(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId()));

	$stu_guard = new \yii\db\Query();
	$stu_guard -> select('*')
	        ->from('stu_academics sg')
		->where('sg.guardia_stu_master_id = '.$_REQUEST['id']);

	$command = $stu_guard->createCommand();
	$stu_data = $command->queryAll();
        ?>

<?php if($model->stuMasterCourse->course_id === 3){?>



<div class="row">
  <div class="col-xs-12 col-md-12 col-lg-12">
	<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> <?= Html::encode('Academic Details') ?>
	<div class="pull-right">
	<?php if(Yii::$app->user->can("/student/stu-master/update") && ($_REQUEST['id'] == Yii::$app->session->get('stu_id'))  || (in_array("SuperAdmin", $adminUser)) || Yii::$app->user->can("updateAllStuInfo")) { ?>
		<?= Html::a('<i class="fa fa-pencil-square-o"></i> Edit', ['update', 'sid' => $model->stu_master_id, 'tab' => 'academic'], ['class' => 'btn-sm btn btn-primary text-warning', 'id' => 'update-data']) ?>
	<?php } ?>
	</div>
	</h2>
  </div><!-- /.col -->
</div>
<h1>First Standard</h1>
<table>
      <!-- sem row-->
               <tr>
                   <th></th>
                   <th colspan="4" > sem 1</th>
                    <th colspan="4">sem 2</th>
                     <th rowspan="2">TOTAL GRADE</th>
               </tr>
               
                <tr>
    <th>Subjects</th>
    <th>FA 1</th>
    <th>FA 2</th>
    <th>SA 1</th>
    <th>Total</th>
    <th>FA 3</th>
    <th>FA 4</th>
    <th>SA 2</th>
    <th>TOTAL</th>
   
  </tr>
   <!-- percentage row-->
   <tr>
       <th> </th>
    <th>10%</th>
    <th>10%</th>
    <th>30%</th>
    <th>50%</th>
    <th>10%</th>
    <th>10%</th>
    <th>30%</th>
    <th>50%</th>
    <th>100%</th>
   
  </tr>
   <!-- First language row row-->
  <tr>
    <td>First Language</td>
    <td><?= $model->first_language ?></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
    <!-- Secound Language Row -->
  <tr>
    <td>Secound Language</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Third Language row-->
  <tr>
    <td>Third Language</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Mathematics row-->
  <tr>
    <td>Mathematics</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Science row-->
  <tr>
    <td>Science</td>
     <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Social Science row-->
  <tr>
    <td>Social Science</td>
     <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Physical and health edication row-->
  <tr>
    <td>Physical Education</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <!-- Drawing and work experience row-->
  <tr>
    <td>Drawing</td>
     <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <!-- computer education row-->
  <tr>
    <td>Computer Education</td>
     <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
       <table>
            <tr>
                <th colspan="3"><h3>Co - Scholastic achievement</h3></th>
            </tr>
            <tr>
                <th>Description</th>
                <th>First Sem Grade</th>
                <th>Secound Sem Grade</th>
            </tr>
            <tr>
                <td>Emotional And Social Skills</td>
                <td></td>
                <td></td>
               
            </tr>
             <tr>
               
                <td>Organisational Skills</td>
                <td></td>
                <td></td>
                
            </tr>
             <tr>
               
                <td>Scientific Skills</td>
                <td></td>
                <td></td>
               
            </tr>
             <tr>
               
                <td>Fine Arts</td>
                <td></td>
                <td></td>
               
            </tr>
             <tr>
               
                <td>Creativity</td>
                <td></td>
                <td></td>
            </tr>
             <tr>
               
                <td>Attitude</td>
                <td></td>
                <td></td>
            </tr>
             <tr>
               
                <td>Value</td>
                <td></td>
                <td></td>
            </tr>
           
        </table>



<?php } elseif(!empty ($stu_data)) {
 
?>



<?php
     if(!empty($stu_data))
     {	
	$i = 1;
	foreach($stu_data as $sd)
	{	
		if($sd['is_emg_contact'] == 1)
			$check = true; 
		else
			$check = false;
                if($sd['guardian_mobile_no'] === '3') {
?>

<div class="row">
  <div class="col-xs-12 col-md-12 col-lg-12">
	<h2 class="page-header edusec-border-bottom-warning">
            
            <h1> First Standard </h1>
	<div class="pull-right">
		<?php if(Yii::$app->user->can("/student/stu-master/update") && ($_REQUEST['id'] == Yii::$app->session->get('stu_id')) || (in_array("SuperAdmin", $adminUser)) || Yii::$app->user->can("updateAllStuInfo")) { ?>
		    <?= Html::a('<i class="fa fa-pencil-square-o"></i> Edit', '#', ['class' => 'btn btn-primary btn-sm', 'onclick' => "updateGuard(".$sd['stu_guardian_id'].",".$model->stu_master_id.", 'guardians');return false;"]) ?>
		<?php } ?>
		<?php if(Yii::$app->user->can('/student/stu-guardians/delete') && ($_REQUEST['id'] == Yii::$app->session->get('stu_id')) || (in_array("SuperAdmin", $adminUser)) || Yii::$app->user->can("updateAllStuInfo")) { ?>
		    <?= Html::a('<i class="fa fa-trash-o"></i> Delete', ['stu-guardians/delete', 'id' => $sd['stu_guardian_id'], 'sid'=>$model->stu_master_id], [
			'class' => 'btn btn-danger btn-sm',
			'data' => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method' => 'post',
			],
		    ]) ?> 
		<?php } ?>
        </div>
	</h2>
  </div><!-- /.col -->
</div>

<?php
// **** Update Status of is_emp_contact on student Guardian tab ****
$url = yii\helpers\Url::toRoute(["stu-master/emg-change-status"]);
$JSClick = <<<EOF
function(event, state) {
	var st_id = this.value;
	var sid = $( this ).attr( "sid" );
	var guard_id = $( this ).attr( "guard_id" );	
	$.ajax({
		type: "POST",
		url: "{$url}",
		data: { sid: sid, guard_id: guard_id },
		success: function(result){
			//window.location = 'index';
		}
	});
}
EOF;
?>
<div class="row">
  <div class="col-xs-12 col-md-12 col-lg-12">
	<div class="pull-right edusec-stu-emg-gur">
		<!--<span class="edusec-emg-ct-label">Is Emergency Contact</span>-->
		<?php if((Yii::$app->user->can('/student/stu-master/emg-change-status') && ($_REQUEST['id'] == Yii::$app->session->get('stu_id'))) || ( $adminUser == "SuperAdmin" ) || Yii::$app->user->can("updateAllStuInfo")) { ?>
		  
		<?php } else { 
			echo (($sd['is_emg_contact'] == 1) ? "<span class='label label-success'>Yes</span>" : "<span class='label label-warning'>No</span>");
		}?>
        </div>
  </div><!-- /.col -->
</div>

<div class="row">

	
	
	
	
</div>
<table>
               <!-- sem row-->
               <tr>
                   <th></th>
                   <th colspan="4" > sem 1</th>
                    <th colspan="4">sem 2</th>
                     <th rowspan="2">TOTAL GRADE</th>
               </tr>
                <!-- subject, test row row-->
  <tr>
    <th>Subjects</th>
    <th>FA 1</th>
    <th>FA 2</th>
    <th>SA 1</th>
    <th>Total</th>
    <th>FA 3</th>
    <th>FA 4</th>
    <th>SA 2</th>
    <th>TOTAL</th>
   
  </tr>
   <!-- percentage row-->
   <tr>
       <th> </th>
    <th>10%</th>
    <th>10%</th>
    <th>30%</th>
    <th>50%</th>
    <th>10%</th>
    <th>10%</th>
    <th>30%</th>
    <th>50%</th>
    <th>100%</th>
   
  </tr>
   <!-- First language row row-->
  <tr>
    <td>First Language</td>
    <td><?= $sd['guardian_phone_no'] ?></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Secound Language Row -->
  <tr>
    <td>Secound Language</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Third Language row-->
  <tr>
    <td>Third Language</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Mathematics row-->
  <tr>
    <td>Mathematics</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Science row-->
  <tr>
    <td>Science</td>
     <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Social Science row-->
  <tr>
    <td>Social Science</td>
     <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
   <!-- Physical and health edication row-->
  <tr>
    <td>Physical Education</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <!-- Drawing and work experience row-->
  <tr>
    <td>Drawing</td>
     <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <!-- computer education row-->
  <tr>
    <td>Computer Education</td>
     <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
       <table>
            <tr>
                <th colspan="3"><h3>Co - Scholastic achievement</h3></th>
            </tr>
            <tr>
                <th>Description</th>
                <th>First Sem Grade</th>
                <th>Secound Sem Grade</th>
            </tr>
            <tr>
                <td>Emotional And Social Skills</td>
                <td></td>
                <td></td>
               
            </tr>
             <tr>
               
                <td>Organisational Skills</td>
                <td></td>
                <td></td>
                
            </tr>
             <tr>
               
                <td>Scientific Skills</td>
                <td></td>
                <td></td>
               
            </tr>
             <tr>
               
                <td>Fine Arts</td>
                <td></td>
                <td></td>
               
            </tr>
             <tr>
               
                <td>Creativity</td>
                <td></td>
                <td></td>
            </tr>
             <tr>
               
                <td>Attitude</td>
                <td></td>
                <td></td>
            </tr>
             <tr>
               
                <td>Value</td>
                <td></td>
                <td></td>
            </tr>
           
        </table>

<?php
                }
	$i++;	
	}
    }
    else
    {
?>
<table width="100%" cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-hover table-responsive">
	<tr>
		<th class="table-cell-title text-center" colspan = 4><?= "No Data Available" ?></th>
	</tr>
</table>
<?php
    }
?>
<?php } else {?>
 <table width="100%" cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-hover table-responsive">
	<tr>
		<th class="table-cell-title text-center" colspan = 4><?= "No More Data Available" ?></th>
	</tr>
</table>
<?php } ?>
<select id='purposefirst' class=' btn btn-primary dropdown-toggle'>
<option value="0">pass/fail</option>
<option value="1">pass</option>
<option value="2">fail</option>
</select>
<div id="fs" style="display:none;">
<div class="row">
  <div class="col-xs-12 col-md-12 col-lg-12">
	
	<div class="pull-right">
	<?php if(Yii::$app->user->can('/student/stu-master/addguardian')) { ?>
		<?= Html::a('<i class="fa fa-user-plus"></i> Re-Admission', ['addcourse', 'sid' => $model->stu_master_id], ['class' => 'btn-sm btn btn-primary text-warning', 'id' => 'update-guard-data']) ?>
	<?php } ?>
             <button type="button" class="btn-sm btn btn-success text-success">Alumni</button>
	</div>
	
	
  </div><!-- /.col -->
</div>
</div>
       <div id="sf" style="display:none;">
<div class="row">
  <div class="col-xs-12 col-md-12 col-lg-12">
	
	<div class="pull-right">
	<?php if(Yii::$app->user->can('/student/stu-master/addguardian')) { ?>
		<?= Html::a('<i class="fa fa-user-plus"></i> Re-Admission', ['addcourse', 'sid' => $model->stu_master_id], ['class' => 'btn-sm btn btn-primary text-warning', 'id' => 'update-guard-data']) ?>
	<?php } ?>
             <button type="button" class="btn-sm btn btn-success text-success">Alumni</button>
	</div>
	
  </div><!-- /.col -->
</div>
</div>

<!-- scripts -->
<script>
     
        $(document).ready(function(){
    $('#purposefirst').on('change', function() {
      if ( this.value == '1')
      
      {
        $("#fs").show();
      }
      else
      {
        $("#fs").hide();
      }
    });
});

  $(document).ready(function(){
    $('#purposefirst').on('change', function() {
      if ( this.value == '2')
      
      {
          var r = confirm("Are you sure, if you proceed student's current data will be not editable ,and he will continue wih with the same class");
if (r == true) {
    x = "You pressed OK!";
     $("#sf").show();
} else {
    x = "You pressed Cancel!";
}
       
      }
      else
      {
        $("#sf").hide();
      }
    });
});

 
 
 </script>