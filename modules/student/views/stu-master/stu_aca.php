<?php

use yii\helpers\Html;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $guard app\modules\student\models\StuGuardians */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-xs-12 col-lg-12">
  <div class="<?php echo $guard->isNewRecord ? 'box-success' : 'box-info'; ?> box view-item col-xs-12 col-lg-12">
   <div class="stu-guardians-form">
    <?php $form = ActiveForm::begin([
			'id' => 'stu-academics-form',
			'fieldConfig' => [
			    'template' => "{label}{input}{error}",
			],
    ]); ?>

   <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
  
    <div class="col-xs-12 col-sm-6 col-lg-6">
     <?= $form->field($guard, 'guardian_mobile_no')->dropDownList(ArrayHelper::map(app\modules\course\models\Courses::find()->where(['is_status' => 0])->all(),'course_id','course_name'),[
                   
                    'prompt'=>'---Select Course---',
                    'onchange'=>'
                        $.get( "'.Url::toRoute('dependent/studbatch').'", { id: $(this).val() } )
                            .done(function( data ) {
                              
                            }
                        );
                    '    
                ]);?>
       
    </div>
       
    </div>
   </div>

   <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
   

   </div>


    <div class="form-group col-xs-12 col-sm-6 col-lg-4 no-padding">
	<div class="col-xs-6">
        <?= Html::submitButton($guard->isNewRecord ? 'Add' : 'Update', ['class' => $guard->isNewRecord  ? 'btn btn-block btn-success' : 'btn btn-block btn-info']) ?>
	</div>
	<div class="col-xs-6">
	    <?= Html::a('Cancel', ['stu-master/view', 'id' => $model->stu_master_id, '#' => 'guardians'], ['class' => 'btn btn-default btn-block']); ?>
	</div>
    </div>
    <?php ActiveForm::end(); ?>
   </div>
  </div>
</div>
