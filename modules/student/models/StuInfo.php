<?php
/*****************************************************************************************
 * EduSec  Open Source Edition is a School / College management system developed by
 * RUDRA SOFTECH. Copyright (C) 2010-2015 RUDRA SOFTECH.

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses. 

 * You can contact RUDRA SOFTECH, 1st floor Geeta Ceramics, 
 * Opp. Thakkarnagar BRTS station, Ahmedbad - 382350, India or
 * at email address info@rudrasoftech.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * RUDRA SOFTECH" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by RUDRA SOFTECH".
 *****************************************************************************************/
/**
 * @package EduSec.modules.student.model
 */

namespace app\modules\student\models;

use Yii;
use app\models\City;
use app\models\State;
use app\models\Country;
use app\models\NewCountry;

/**
 * This is the model class for table "stu_info".
 *
 * @property integer $stu_info_id
 * @property string $stu_unique_id
 * @property string $stu_title
 * @property string $stu_first_name
 * @property string $stu_middle_name
 * @property string $stu_last_name
 * @property string $stu_gender
 * @property string $stu_dob
 * @property string $stu_email_id
 * @property string $stu_bloodgroup
 * @property string $stu_birthplace
 * @property string $stu_religion
 * @property string $stu_admission_date
 * @property string $stu_photo
 * @property string $stu_languages
 * @property string $stu_mobile_no
 * @property integer $stu_info_stu_master_id
 *
 * @property StuMaster $stuInfoStuMaster
 * @property StuMaster[] $stuMasters
 */
class StuInfo extends \yii\db\ActiveRecord
{
	const TYPE_MALE='MALE';
	const TYPE_FEMALE='FEMALE';
	const TYPE_APLUS='A+';
	const TYPE_AMINUS='A-';
	const TYPE_BPLUS='B+';
	const TYPE_BMINUS='B-';
	const TYPE_ABPLUS='AB+';
	const TYPE_ABMINUS='AB-';
	const TYPE_OPLUS='O+';
	const TYPE_OMINUS='O-';
	const TYPE_UNKNON='Unknown';
	const TYPE_MR='Mr.';
	const TYPE_MRS='Mrs.'; 
	const TYPE_MISS='Ms.';
        
   //  var  $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antartica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Ashmore and Cartier Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Clipperton Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo, Democratic Republic of the", "Congo, Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czeck Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Europa Island", "Falkland Islands (Islas Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern and Antarctic Lands", "Gabon", "Gambia, The", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Glorioso Islands", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Howland Island", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Ireland, Northern", "Israel", "Italy", "Jamaica", "Jan Mayen", "Japan", "Jarvis Island", "Jersey", "Johnston Atoll", "Jordan", "Juan de Nova Island", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Man, Isle of", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcaim Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romainia", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and South Sandwich Islands", "Spain", "Spratly Islands", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Tobago", "Toga", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "USA", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wales", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stu_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             [['e_first_name','e_middle_name','e_last_name','e_housename','e_streetname'],'string'],
			[['e_city','e_state','e_country','g_city','g_state','g_country'],'string'],
            [['e_mobilenum','e_phonenum','e_housenum','e_pincode'],'integer'],
            [['g_name','g_relation','g_occupation'],'string'],
            [['g_qulification'],'string'],
            [['g_mobilenum'],'integer'],
            [['stu_unique_id', 'stu_first_name', 'stu_last_name', 'stu_admission_date','stu_dice_num'], 'required', 'message' => ''],
	    [['stu_unique_id'], 'unique'],
            
            [['upload_date','g_homeaddress','g_officeaddress','e_address','house_name','income_category'],'safe'],
            [['house_no','phone','fax_no','office_no','g_phonenum','street_name'],'safe'],
           // [['stu_admission_date'], 'string', 'max' => 15],
	    [['stu_info_stu_master_id'], 'safe'],
	    ['stu_dob', 'required', 'message' => ''],
	  // ['stu_dob','Ckdate'],
            [['stu_mobile_no', 'stu_info_stu_master_id'], 'integer'],
            //[['stu_unique_id'], 'string', 'max' => 50],
	    [['stu_mobile_no'], 'integer', 'min' => 10],
            [['stu_first_name', 'stu_middle_name', 'stu_last_name', 'stu_religion'], 'string', 'max' => 50],
            [['stu_title'], 'string', 'max' => 5],
            [['stu_gender', 'stu_bloodgroup'], 'string', 'max' => 10],
           // [['stu_email_id'], 'string', 'max' => 65],
            [['stu_birthplace'], 'string', 'max' => 45],
            [['stu_photo'], 'file', 'extensions' => 'jpg, jpeg, gif, png', 'skipOnEmpty' => false, 
'checkExtensionByMimeType'=>false, 'on' => 'photo-upload'],
            [['stu_languages'], 'string', 'max' => 255],
	    [['stu_email_id'], 'email'],
           // [['stu_email_id'], 'unique'],
            [['stu_email_id','stu_mobile_no'], 'required' ,'message' => ''],
           
            [['taluk','district','mother_tongue'],'string','max' => 50],
            [['father_first_name','father_middle_name','father_last_name','mother_first_name','mother_middle_name','mother_last_name'],'string','max' => 50],
            [['annual_income','no_dependents'], 'integer'],
            [['pincode'],'integer'],
            [['country','state','city'],'string','max' => 50],
            [['file_number'],'safe'],
            [['contact_landline_std','contact_fax_std','guardian_landline_std','guardian_country_mobile','emergency_landline_std','emergency_country_mobile'],'safe'],
            [['e_firstname'],'string','max' => 50]
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'stu_info_id' => 'Stu Info ID',
            'stu_unique_id' => 'LoginNumber',
            'stu_title' => 'Title',
            'stu_first_name' => 'First Name',
            'stu_middle_name' => 'Middle Name',
            'stu_last_name' => 'Last Name',
            'stu_gender' => 'Gender',
            'stu_dob' => 'Birth Date',
            'stu_email_id' => 'Email ID',
            'stu_bloodgroup' => 'Bloodgroup',
            'stu_birthplace' => 'Birthplace',
            'stu_religion' => 'Religion',
            'stu_admission_date' => 'Admission Date',
            'stu_photo' => 'Photo',
            'stu_languages' => 'Languages',
            'stu_mobile_no' => 'Mobile No',
            'stu_info_stu_master_id' => 'Stu Info Stu Master ID',
            'stu_dice_num' => 'DICE Number',
            'stu_adm_num' => 'Admission number',
            'annual_income' => 'Parent\'s Annual Income',
            'office_no' => 'Office Number',
            'phone' => 'Land Line Number',
            'g_name' => 'Name',
            'g_relation' => 'Relation',
            'g_occupation' => 'Occupation',
            'g_qulification' => 'Qualification',
            'g_city' => 'City',
            'g_state' => 'State',
            'g_country' => 'Country',
            'g_phonenum' => 'Land Line Number',
            'g_mobilenum' => 'Mobile Number',
            'g_homeaddress' => 'Home Address',
            'g_officeaddress' => 'Office Address',
            'e_firstname' => 'First Name',
            'e_middle_name' => 'Middle Name',
            'e_last_name' => 'Last Name',
            'e_housenum' => 'House Number',
            'e_housename' => 'House Name',
            'e_streetname' => 'Street Name',
            'e_city' => 'City',
            'e_state' => 'State',
            'e_country' => 'Country',
            'e_pincode' => 'Pincode',
            'e_phonenum' => 'Land Line Number',
            'e_mobilenum' => 'Mobile Number',
            'e_address' => 'Address',
            'upload_date' => 'Date On File',
            'no_dependents' => 'Number Of Dependents',
            'house_no' => 'House Number',
            'house_name' => 'Address',
            'city' => 'City',
            'state' => 'State',
            'country' => 'Country',
            'contact_landline_std' => 'STD',
            'contact_fax_std' => 'STD',
            'guardian_landline_std' => 'STD',
            'guardian_country_mobile' => 'Country Code',
            'emergency_landline_std' => 'STD',
            'emergency_country_mobile' => 'Country Code',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStuInfoStuMaster()
    {
        return $this->hasOne(StuMaster::className(), ['stu_master_id' => 'stu_info_stu_master_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStuMasters()
    {
        return $this->hasMany(StuMaster::className(), ['stu_master_stu_info_id' => 'stu_info_id']);
    }

    public static function getTitleOptions()
    {
	return [
		self::TYPE_MR=>'Mr.',
		self::TYPE_MRS=>'Mrs.', 
		self::TYPE_MISS=>'Ms.',
	];
    }

    public static function getBloodGroup()
    {
	return [
		self::TYPE_UNKNON => 'Unknown',
		self::TYPE_APLUS => 'A+',
		self::TYPE_AMINUS => 'A-',
		self::TYPE_BPLUS => 'B+',
		self::TYPE_BMINUS => 'B-',
		self::TYPE_ABPLUS => 'AB+',
		self::TYPE_ABMINUS => 'AB-',
		self::TYPE_OPLUS => 'O+',
		self::TYPE_OMINUS => 'O-',
	];
    }
   
    public static function populateCountries()
            
    {

        return[
            
          //  self::$countries,
            
            
        ];
        
    }

    public static function getStuPhoto($imgName)
    {
		$dispImg = is_file(Yii::getAlias('@webroot').'/data/stu_images/'.$imgName) ? true :false;
		return Yii::getAlias('@web')."/data/stu_images/".(($dispImg) ? $imgName : "no-photo.png");
    }

    public function Ckdate($attr,$param) {
		
	$curr_date = date('d-m-Y');
	$dob = date('Y-m-d',strtotime($this->$attr));
	$adm_date = date('Y-m-d',strtotime($this->stu_admission_date));
	
	if(empty($this->stu_admission_date)) {
		return true;
	}
	else {
	  $diff = $adm_date - $dob;
	  $d = date('Y-m-d',strtotime($this->stu_admission_date)) - date('Y-m-d',strtotime($this->$attr));
	   if($d <= 14) {
	
		$this->addError($attr, "Birth date must be less than Admission date.");	
		return false;
	   }
	   else
		return true;
	}
    }
    
//     public function getStuCaddCity()
//    {
//        return $this->hasOne(City::className(), ['city_id' => 'city']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getStuCaddState()
//    {
//        return $this->hasOne(State::className(), ['state_id' => 'state']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getStuCaddCountry()
//    {
//        return $this->hasOne(Country::className(), ['country_id' => 'country']);
//    }


    function getName() {
	return $this->stu_first_name.' '.$this->stu_last_name;
    }
    
  
}
