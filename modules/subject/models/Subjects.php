<?php
/*****************************************************************************************
 * EduSec  Open Source Edition is a School / College management system developed by
 * RUDRA SOFTECH. Copyright (C) 2010-2015 RUDRA SOFTECH.

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses. 

 * You can contact RUDRA SOFTECH, 1st floor Geeta Ceramics, 
 * Opp. Thakkarnagar BRTS station, Ahmedbad - 382350, India or
 * at email address info@rudrasoftech.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * RUDRA SOFTECH" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by RUDRA SOFTECH".
 *****************************************************************************************/

/**
 * This is the model class for table "course".
 *
 * @package EduSec.modules.course.models
 */

namespace app\modules\subject\models;

use Yii;
use app\models\User;
/**
 * This is the model class for table "courses".
 *
 * @property integer $course_id
 * @property string $course_name
 * @property string $course_code
 * @property string $course_alias
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $is_status
 *
 * @property Batches[] $batches
 * @property Users $createdBy
 * @property Users $updatedBy
 * @property StuAdmissionMaster[] $stuAdmissionMasters
 * @property StuMaster[] $stuMasters
 */
class Subjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subjects';
    }

    public static function find()
    {
	return parent::find()->andWhere(['<>', 'is_status', 2]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             [['subject_name', 'subject_course_id', 'subject_alias', 'created_at', 'created_by'], 'required', 'message' => ''],
            [['subject_course_id', 'created_by', 'updated_by', 'is_status'], 'integer', 'message' => ''],
            [['created_at', 'updated_at'], 'safe', 'message' => ''],
            [['subject_name'], 'string', 'max' => 120],
            [['subject_alias'], 'string', 'max' => 35],
	    [['subject_name', 'subject_course_id'], 'unique', 'targetAttribute' => ['subject_name', 'subject_course_id'], 'message' => 'subject Already Exists for this Course.', 'when' => function ($model){ return $model->is_status == 0;}],
            [['subject_alias'], 'unique', 'targetAttribute' => ['subject_alias'],]
           ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
           ];
    }
 /**
     * @return \yii\db\ActiveQuery
     */
    public function getBatchCourse()
    {
        return $this->hasOne(Courses::className(), ['course_id' => 'batch_course_id']);
    }
   
}
